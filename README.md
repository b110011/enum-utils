# Enum Utils

Header-only C++17 library provides static reflection for enums.

## Features

* C++17
* Header-only
* Dependency-free
* Compile-time
* String to enum
* Iterating over enum

## Example usage

To build the provided example you need to do the below commands in your terminal.

```bash
$ cmake -S. -Bbuild -DCMAKE_INSTALL_PREFIX=install
$ cmake --build build/ --target install
```

## Dependencies

This library has no other dependencies than the [C++ standard library](http://en.cppreference.com/w/cpp/header).

## Installation and use

This library is a single-file header-only library. Put content of the [include](include/) folder directly into the project source tree or somewhere reachable from your project.

## License

This library is distributed under the [Apache License, Version 2.0](LICENSE).
