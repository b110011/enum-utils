from conans import ConanFile, CMake

class EnumUtilsConan(ConanFile):
    version = "1.0.0"
    name = "Enum Utils"
    description = "Header-only C++17 library provides static reflection for enums."
    license = "Boost Software License - Version 1.0. http://www.boost.org/LICENSE_1_0.txt"
    url = "https://gitlab.com/b110011/enum-utils.git"
    exports_sources = "include/b110011/*", "CMakeLists.txt", "cmake/*", "LICENSE.txt"
    settings = "compiler", "build_type", "arch"
    build_policy = "missing"
    author = "Serhii Zinchenko"

    def build(self):
        """Avoid warning on build step"""
        pass

    def package(self):
        """Run CMake install"""
        cmake = CMake(self)
        cmake.configure()
        cmake.install()

    def package_info(self):
        self.info.header_only()
