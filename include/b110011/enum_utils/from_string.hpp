/*
Enum Utils: Header-only C++17 library provides static reflection for enums.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef __B110011_ENUMUTILS_FROMSTRING_HPP__
#define __B110011_ENUMUTILS_FROMSTRING_HPP__

#include "b110011/enum_utils/iterator.hpp"
#include "b110011/string/comparators.hpp"
#include "b110011/string/hashers.hpp"

#include <optional>
#include <unordered_map>

namespace b110011 {
namespace enum_utils {

template< typename _EnumType >
using EnumToStringConverter = std::function< std::string_view ( _EnumType ) >;

namespace {

template< typename _EnumType, bool _IsCaseInsensitive = false >
class EnumFromStringConverter final
{

public:

    EnumFromStringConverter (
        _EnumType _begin,
        _EnumType _end,
        EnumToStringConverter< _EnumType > _toString
    )
    {
        EnumIterator< _EnumType >::forEach(
            _begin,
            _end,
            [ this, _toString ] ( _EnumType _element ) -> bool
            {
                m_values.emplace( _toString( _element ), _element );
                return true;
            }
        );
    }

    EnumFromStringConverter ( EnumFromStringConverter const & ) = delete;

    EnumFromStringConverter ( EnumFromStringConverter && ) = delete;

    [[nodiscard]]
    std::optional< _EnumType > fromString ( std::string_view _str ) const
    {
        if ( auto const it{ m_values.find( _str ) }; it != m_values.cend() )
        {
            return it->second;
        }

        return std::nullopt;
    }

private:

    std::unordered_map<
        std::string_view,
        _EnumType,
        StringHasher< _IsCaseInsensitive >,
        IsStringEqual< _IsCaseInsensitive >
    > m_values;

};

} // namespace

template<
    typename _EnumType,
    _EnumType _Begin,
    _EnumType _End,
    bool _IsCaseInsensitive = false
>
[[nodiscard]]
std::optional< _EnumType >
getEnumValueFromString (
    EnumToStringConverter< _EnumType > _toString,
    std::string_view _str
)
{
    static_assert(
        std::is_enum_v< _EnumType >,
        "This template function works only with enums!"
    );

    using Converter = EnumFromStringConverter< _EnumType, _IsCaseInsensitive >;
    static Converter const s_converter{ _Begin, _End, _toString };

    return s_converter.fromString( _str );
}

} // namespace enum_utils
} // namespace b110011

#endif // __B110011_ENUMUTILS_FROMSTRING_HPP__
