/*
Enum Utils: Header-only C++17 library provides static reflection for enums.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef __B110011_ENUMUTILS_ITEMSCOUNT_HPP__
#define __B110011_ENUMUTILS_ITEMSCOUNT_HPP__

#include "b110011/enum_utils/type.hpp"
#include "b110011/math.hpp"

namespace b110011 {
namespace enum_utils {

template< typename _EnumType >
class EnumItemsCount final
{

private:

    using StraightType = std::decay_t< std::remove_pointer_t< _EnumType > >;

    using UnderlyingType = std::underlying_type_t< StraightType >;

    using SizeType = std::common_type_t< std::size_t, UnderlyingType >;

    template< typename _E, EnumTypes _EnumTypeValue >
    struct Counter
    {
        static constexpr auto Value{ EnumItemsCount::InvalidValue };
    };

    template< typename _E >
    struct Counter< _E, EnumTypes::Enumeration >
    {
        static_assert(
            std::is_unsigned_v< UnderlyingType > || static_cast< UnderlyingType >( _E::Count ) > 0,
            "EnumItemsCount: 'Count' enum element cannot have negative value!"
        );

        static constexpr auto Value{ static_cast< SizeType >( _E::Count ) };
    };

    template< typename _E >
    struct Counter< _E, EnumTypes::Mask >
    {
        static constexpr auto isFirstMask = math::isPowOf2( static_cast< UnderlyingType >( _E::First ) );
        static constexpr auto isLastMask = math::isPowOf2( static_cast< UnderlyingType >( _E::Last ) - 1 );

        static_assert(
            std::is_unsigned_v< UnderlyingType > || ( isFirstMask && isLastMask ),
            "EnumItemsCount: 'First' and 'Last' enum elements must be of power of 2!"
        );

        static constexpr auto log2First = math::log2( static_cast< SizeType >( _E::First ) ) + 1;
        static constexpr auto log2Last = math::log2( static_cast< SizeType >( _E::Last ) );

        static constexpr auto Value{ log2Last - log2First };
    };

    static constexpr auto getEnumElementsCount () noexcept
    {
        return Counter< StraightType, EnumType< _EnumType >::Value >::Value;
    }

public:

    static constexpr auto InvalidValue{ static_cast< SizeType >( -1 ) };
    static constexpr auto Value{ getEnumElementsCount() };

};

} // namespace enum_utils
} // namespace b110011

#endif // __B110011_ENUMUTILS_ITEMSCOUNT_HPP__
