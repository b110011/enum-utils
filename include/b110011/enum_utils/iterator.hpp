/*
Enum Utils: Header-only C++17 library provides static reflection for enums.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef __B110011_ENUMUTILS_ITERATOR_HPP__
#define __B110011_ENUMUTILS_ITERATOR_HPP__

#include "b110011/enum_utils/type.hpp"

#include <functional>

namespace b110011 {
namespace enum_utils {
namespace {

template< EnumTypes _enumType >
struct IterationPolicy;

template<>
struct IterationPolicy< EnumTypes::Enumeration >
{
    template< typename _EnumT >
    static constexpr void increase ( _EnumT & _value ) noexcept
    {
        using UnderlyingType = typename std::underlying_type_t< _EnumT >;
        _value = static_cast< _EnumT >( static_cast< UnderlyingType >( _value ) + 1 );
    }
};

template<>
struct IterationPolicy< EnumTypes::Mask >
{
    template< typename _EnumT >
    static constexpr void increase ( _EnumT & _value ) noexcept
    {
        using UnderlyingType = typename std::underlying_type_t< _EnumT >;
        _value = static_cast< _EnumT >( static_cast< UnderlyingType >( _value ) << 1 );
    }
};

} // namespace

template< typename _EnumT >
class EnumIterator final
{

public:

    using Callback = std::function< bool ( _EnumT ) >;

    static bool forEach ( _EnumT _begin, _EnumT _end, Callback _callback )
    {
        using Policy = IterationPolicy< EnumType< _EnumT >::Value >;
        for ( auto i{ _begin }; i < _end; Policy::increase( i ) )
        {
            if ( !_callback( i ) )
            {
                return false;
            }
        }

        return true;
    }

};

} // namespace enum_utils
} // namespace b110011

#endif // __B110011_ENUMUTILS_ITERATOR_HPP__
