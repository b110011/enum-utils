/*
Enum Utils: Header-only C++17 library provides static reflection for enums.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef __B110011_ENUMUTILS_TYPE_HPP__
#define __B110011_ENUMUTILS_TYPE_HPP__

#include <type_traits>

namespace b110011 {
namespace enum_utils {

enum class EnumTypes
{
    Unknown,
    Enumeration,
    Mask
};

template< typename _EnumType >
class EnumType final
{

private:

    template< typename _E >
    static constexpr EnumTypes invoke ( ... ) noexcept
    {
        return EnumTypes::Unknown;
    }

    template< typename _E >
    static constexpr EnumTypes invoke ( decltype( _E::Count ) *, ... ) noexcept
    {
        return EnumTypes::Enumeration;
    }

    template< typename _E >
    static constexpr EnumTypes invoke ( decltype( _E::First ) *, decltype( _E::Last ) * ) noexcept
    {
        return EnumTypes::Mask;
    }

    static constexpr auto getEnumType () noexcept
    {
        using RealType = std::decay_t< std::remove_pointer_t< _EnumType > >;
        static_assert( std::is_enum_v< RealType >, "EnumType: Passed type is not an enum!" );

        return invoke< RealType >( nullptr, nullptr );
    }

public:

    static constexpr auto Value{ getEnumType() };

};

} // namespace enum_utils
} // namespace b110011

#endif // __B110011_ENUMUTILS_TYPE_HPP__
