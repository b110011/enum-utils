/*
Enum Utils: Header-only C++17 library provides static reflection for enums.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/*
Source:
    http://www.graphics.stanford.edu/~seander/bithacks.html
    https://hbfs.wordpress.com/2016/03/22/log2-with-c-metaprogramming/
*/

#ifndef __B110011_MATH_HPP__
#define __B110011_MATH_HPP__

#include <type_traits>

namespace b110011 {
namespace math {

template< typename _T >
[[nodiscard]]
static constexpr _T
log2 ( _T _value ) noexcept
{
    static_assert( !std::is_floating_point_v< _T >, "Float points are not supported!" );
    return ( _value < 2 ) ? 1 : ( 1 + log2( _value / 2 ) );
}

template< typename _T >
[[nodiscard]]
static constexpr bool
isPowOf2 ( _T _value ) noexcept
{
    static_assert( !std::is_floating_point_v< _T >, "Float points are not supported!" );
    return _value && !( _value & ( _value - 1 ) );
}

} // namespace math
} // namespace b110011

#endif // __B110011_MATH_HPP__
