/*
Enum Utils: Header-only C++17 library provides static reflection for enums.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef __B110011_STRING_COMPARATORS_HPP__
#define __B110011_STRING_COMPARATORS_HPP__

#include <cstring>
#include <string_view>

namespace b110011 {

template< bool _IsCaseInsensitive >
struct IsStringEqual;

template<>
struct IsStringEqual< false >
{
    [[nodiscard]]
    constexpr bool operator () ( std::string_view _lhs, std::string_view _rhs ) const noexcept
    {
        return _lhs == _rhs;
    }
};

template<>
struct IsStringEqual< true >
{
    [[nodiscard]]
    bool operator () ( std::string_view _lhs, std::string_view _rhs ) const
    {
        if ( _lhs.size() != _rhs.size() )
        {
            return false;
        }

        #ifdef _WIN32
        return ::_strnicmp( _lhs.data(), _rhs.data(), _lhs.size() ) == 0;
        #else
        return strncasecmp( _lhs.data(), _rhs.data(), _lhs.size() ) == 0;
        #endif
    }
};

using CaseInsensitiveComparator = IsStringEqual< true >;
using CaseSensitiveComparator = IsStringEqual< false >;

} // namespace b110011

#endif // __B110011_STRING_COMPARATORS_HPP__
