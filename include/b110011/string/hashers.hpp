/*
Enum Utils: Header-only C++17 library provides static reflection for enums.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef __B110011_STRING_HASHERS_HPP__
#define __B110011_STRING_HASHERS_HPP__

#include <cctype>
#include <string_view>

namespace b110011 {

template< bool _IsCaseInsensitive >
struct StringHasher;

template<>
struct StringHasher< false >
{
    [[nodiscard]]
    std::size_t operator () ( std::string_view _str ) const noexcept
    {
        return std::hash< std::string_view >{}( _str );
    }
};

template<>
struct StringHasher< true >
{
    [[nodiscard]]
    constexpr std::size_t operator () ( std::string_view _str ) const noexcept
    {
        std::size_t hash{ 0 };

        for ( auto const ch : _str )
        {
            hash = hash * 31 + std::toupper( ch );
        }

        return hash;
    }
};

using CaseInsensitiveHasher = StringHasher< true >;
using CaseSensitiveHasher = StringHasher< false >;

} // namespace b110011

#endif // __B110011_STRING_HASHERS_HPP__
