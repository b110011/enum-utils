/*
Enum Utils: Header-only C++17 library provides static reflection for enums.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef __B110011_TESTS_CATCH2_HPP__
#define __B110011_TESTS_CATCH2_HPP__

#ifndef CATCH_VERSION_MAJOR
    #include <catch2/catch.hpp>
#endif

#ifdef TEST_CASE
    #undef TEST_CASE
#else
    #error Catch2 library does not define TEST_CASE macro!
#endif

#ifdef TEST_CASE_METHOD
    #undef TEST_CASE_METHOD
#else
    #error Catch2 library does not define TEST_CASE_METHOD macro!
#endif

#ifdef METHOD_AS_TEST_CASE
    #undef METHOD_AS_TEST_CASE
#else
    #error Catch2 library does not define METHOD_AS_TEST_CASE macro!
#endif

#ifdef REGISTER_TEST_CASE
    #undef REGISTER_TEST_CASE
#else
    #error Catch2 library does not define REGISTER_TEST_CASE macro!
#endif

#ifdef B110011_TEST_CASE_PREFIX
    #define __B110011_TOSTRING__( _value ) __B110011_TOSTRING2__( _value )
    #define __B110011_TOSTRING2__( _value ) #_value

    #define __B110011_CATCH_TEST_CASE_PREFIX__ "[" __B110011_TOSTRING__( B110011_TEST_CASE_PREFIX ) "] "
#else
    #define __B110011_CATCH_TEST_CASE_PREFIX__
#endif

#define TEST_CASE( ... ) \
    INTERNAL_CATCH_TESTCASE( __B110011_CATCH_TEST_CASE_PREFIX__ __VA_ARGS__ )

#define TEST_CASE_METHOD( CLASSNAME, ... ) \
    INTERNAL_CATCH_TEST_CASE_METHOD( CLASSNAME, __B110011_CATCH_TEST_CASE_PREFIX__ __VA_ARGS__ )

#define METHOD_AS_TEST_CASE( METHOD, ... ) \
    INTERNAL_CATCH_METHOD_AS_TEST_CASE( METHOD, __B110011_CATCH_TEST_CASE_PREFIX__ __VA_ARGS__ )

#define REGISTER_TEST_CASE( FUNCTION, ... ) \
    INTERNAL_CATCH_REGISTER_TESTCASE( FUNCTION, __B110011_CATCH_TEST_CASE_PREFIX__ __VA_ARGS__ )

#endif // __B110011_TESTS_CATCH2_HPP__
