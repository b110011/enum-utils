# Enum Utils: Header-only C++17 library provides static reflection for enums.
#
# Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#------------------------------------------------------------------------------#
# Catch2 installation                                                          #
#------------------------------------------------------------------------------#

add_library(catch_main STATIC)

target_compile_features(catch_main
  PUBLIC
    cxx_std_17
)
target_sources(catch_main
  PRIVATE
    test_main.cpp
)

target_link_libraries(catch_main
  PUBLIC
    B110011::enum-utils
)

find_package(Catch2 QUIET)

if(Catch2_FOUND)
  target_link_libraries(catch_main
    PUBLIC
      Catch2::Catch2
  )
else()
  message("Catch2 not found, installing with Conan...")
  if(NOT EXISTS "${CMAKE_BINARY_DIR}/conan.cmake")
    message( STATUS "Downloading conan.cmake from https://github.com/conan-io/cmake-conan")
    file(DOWNLOAD "https://github.com/conan-io/cmake-conan/raw/v0.15/conan.cmake" "${CMAKE_BINARY_DIR}/conan.cmake")
  endif()

  include(${CMAKE_BINARY_DIR}/conan.cmake)
  conan_add_remote(
    NAME
      bincrafters
    URL
      https://api.bintray.com/conan/bincrafters/public-conan
  )
  conan_cmake_run(
    REQUIRES
      catch2/2.11.0
    BASIC_SETUP
    CMAKE_TARGETS # individual targets to link to
    BUILD
      missing
  )

  target_link_libraries(catch_main
    PUBLIC
      CONAN_PKG::catch2
  )
endif()

# Prevents the adding of catch test projects to our project
set_property(GLOBAL PROPERTY CTEST_TARGETS_ADDED 1)
include(Catch)

#------------------------------------------------------------------------------#
# Project tests                                                                #
#------------------------------------------------------------------------------#

# This is a "meta" target that is used to collect all tests
add_custom_target(all_tests)

function(target_test)

  set(options)
  set(oneValueArgs TARGET)
  set(multiValueArgs FILES)
  cmake_parse_arguments(TT "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  set(TARGET_UNIT_TESTS "enum-utils-${TT_TARGET}-unit-tests")

  add_executable("${TARGET_UNIT_TESTS}")

  # Make the overall test meta-target depend on this test
  add_dependencies(all_tests "${TARGET_UNIT_TESTS}")

  target_link_libraries("${TARGET_UNIT_TESTS}"
    PRIVATE
      catch_main
  )
  target_sources("${TARGET_UNIT_TESTS}"
    PRIVATE
      "${TT_FILES}"
  )

  catch_discover_tests("${TARGET_UNIT_TESTS}"
    EXTRA_ARGS
      -s --reporter=xml --out=tests.xml
  )

endfunction()

target_test(
  TARGET
    enum
  FILES
    enum/test_items_count.cpp
    enum/test_iterator.cpp
    enum/test_from_string.cpp
    enum/test_type.cpp
)

target_test(
  TARGET
    string
  FILES
    string/test_comparators.cpp
    string/test_hashers.cpp
)
