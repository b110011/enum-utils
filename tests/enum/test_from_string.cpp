/*
Enum Utils: Header-only C++17 library provides static reflection for enums.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "b110011/enum_utils/from_string.hpp"

#define B110011_TEST_CASE_PREFIX EnumFromString
#include "b110011/tests/catch2.hpp"

/*
Test plan:

Done    1. Enumeration
Done        1.1. Case sensitive
Done        1.2. Case insensitive
Done    2. Mask
Done        2.1. Case sensitive
Done        2.2. Case insensitive
*/

namespace b110011 {
namespace enum_utils {

struct TestEnumeration
{
    enum Enum { A, B, C, Count };

    static constexpr std::string_view toString ( Enum _e ) noexcept
    {
        switch ( _e )
        {
            case A: return "A";
            case B: return "B";
            case C: return "C";

            default:
                return {};
        }
    }

    template< bool _IsCaseInsensitive = false >
    static std::optional< Enum > fromString ( std::string_view _str )
    {
        return getEnumValueFromString<
            Enum,
            Enum::A,
            Enum::Count,
            _IsCaseInsensitive
        >( toString, _str );
    }
};

TEST_CASE( "1.1. Case sensitive", "[Utils][Enum][EnumFromString][Enumeration]" )
{
    CHECK( TestEnumeration::fromString( "A" ) == TestEnumeration::A );
    CHECK( TestEnumeration::fromString( "B" ) == TestEnumeration::B );
    CHECK( TestEnumeration::fromString( "C" ) == TestEnumeration::C );
    CHECK( TestEnumeration::fromString( "D" ) == std::nullopt );

    CHECK( TestEnumeration::fromString( "a" ) == std::nullopt );
    CHECK( TestEnumeration::fromString( "b" ) == std::nullopt );
    CHECK( TestEnumeration::fromString( "c" ) == std::nullopt );
    CHECK( TestEnumeration::fromString( "d" ) == std::nullopt );
}

TEST_CASE( "1.2. Case insensitive", "[Utils][Enum][EnumFromString][Enumeration]" )
{
    CHECK( TestEnumeration::fromString< true >( "A" ) == TestEnumeration::A );
    CHECK( TestEnumeration::fromString< true >( "B" ) == TestEnumeration::B );
    CHECK( TestEnumeration::fromString< true >( "C" ) == TestEnumeration::C );
    CHECK( TestEnumeration::fromString< true >( "D" ) == std::nullopt );

    CHECK( TestEnumeration::fromString< true >( "a" ) == TestEnumeration::A );
    CHECK( TestEnumeration::fromString< true >( "b" ) == TestEnumeration::B );
    CHECK( TestEnumeration::fromString< true >( "c" ) == TestEnumeration::C );
    CHECK( TestEnumeration::fromString< true >( "d" ) == std::nullopt );
}

struct TestMask
{
    enum Enum
    {
        A   = 1 << 3,
        B   = 1 << 4,
        C   = 1 << 5,

        Last,
        First   = A
    };

    static constexpr std::string_view toString ( Enum _e ) noexcept
    {
        switch ( _e )
        {
            case A: return "A";
            case B: return "B";
            case C: return "C";

            default:
                return {};
        }
    }

    template< bool _IsCaseInsensitive = false >
    static std::optional< Enum > fromString ( std::string_view _str )
    {
        return getEnumValueFromString<
            Enum,
            Enum::First,
            Enum::Last,
            _IsCaseInsensitive
        >( toString, _str );
    }
};

TEST_CASE( "2.1. Case sensitive", "[Utils][Enum][EnumFromString][Mask]" )
{
    CHECK( TestMask::fromString( "A" ) == TestMask::A );
    CHECK( TestMask::fromString( "B" ) == TestMask::B );
    CHECK( TestMask::fromString( "C" ) == TestMask::C );
    CHECK( TestMask::fromString( "D" ) == std::nullopt );

    CHECK( TestMask::fromString( "a" ) == std::nullopt );
    CHECK( TestMask::fromString( "b" ) == std::nullopt );
    CHECK( TestMask::fromString( "c" ) == std::nullopt );
    CHECK( TestMask::fromString( "d" ) == std::nullopt );
}

TEST_CASE( "2.2. Case insensitive", "[Utils][Enum][EnumFromString][Mask]" )
{
    CHECK( TestMask::fromString< true >( "A" ) == TestMask::A );
    CHECK( TestMask::fromString< true >( "B" ) == TestMask::B );
    CHECK( TestMask::fromString< true >( "C" ) == TestMask::C );
    CHECK( TestMask::fromString< true >( "D" ) == std::nullopt );

    CHECK( TestMask::fromString< true >( "a" ) == TestMask::A );
    CHECK( TestMask::fromString< true >( "b" ) == TestMask::B );
    CHECK( TestMask::fromString< true >( "c" ) == TestMask::C );
    CHECK( TestMask::fromString< true >( "d" ) == std::nullopt );
}

} // namespace enum_utils
} // namespace b110011
