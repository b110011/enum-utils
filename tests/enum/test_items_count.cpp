/*
Enum Utils: Header-only C++17 library provides static reflection for enums.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "b110011/enum_utils/items_count.hpp"

#define B110011_TEST_CASE_PREFIX EnumItemsCount
#include "b110011/tests/catch2.hpp"

/*
Test plan:

Done    1. Unknown
Done    2. Enumeration
Done    3. Mask
*/

namespace b110011 {
namespace enum_utils {

template< typename _Enum >
void testEnumItemsCount ( std::size_t _expectedCount )
{
    CHECK( EnumItemsCount< _Enum >::Value == _expectedCount );
    CHECK( EnumItemsCount< _Enum const >::Value == _expectedCount );

    CHECK( EnumItemsCount< _Enum & >::Value == _expectedCount );
    CHECK( EnumItemsCount< _Enum const & >::Value == _expectedCount );

    CHECK( EnumItemsCount< _Enum * >::Value == _expectedCount );
    CHECK( EnumItemsCount< _Enum const * >::Value == _expectedCount );

    CHECK( EnumItemsCount< _Enum volatile >::Value == _expectedCount );
    CHECK( EnumItemsCount< _Enum volatile const >::Value == _expectedCount );

    CHECK( EnumItemsCount< _Enum volatile & >::Value == _expectedCount );
    CHECK( EnumItemsCount< _Enum volatile const & >::Value == _expectedCount );

    CHECK( EnumItemsCount< _Enum volatile * >::Value == _expectedCount );
    CHECK( EnumItemsCount< _Enum volatile const * >::Value == _expectedCount );
}

TEST_CASE( "1. Unknown", "[Utils][Enum][EnumItemsCount]" )
{
    enum Test { A, B, C };
    testEnumItemsCount< Test >( EnumItemsCount< Test >::InvalidValue );
}

TEST_CASE( "2. Enumeration", "[Utils][Enum][EnumItemsCount]" )
{
    enum Test { A, B, C, Count };
    testEnumItemsCount< Test >( 3 );
}

TEST_CASE( "3. Mask", "[Utils][Enum][EnumItemsCount]" )
{
    enum Test
    {
        A       = 1 << 3,
        B       = 1 << 4,
        C       = 1 << 5,
        D       = 1 << 6,
        E       = 1 << 7,
        F       = 1 << 8,
        G       = 1 << 9,

        Last,
        First   = A
    };

    testEnumItemsCount< Test >( 7 );
}

} // namespace enum_utils
} // namespace b110011
