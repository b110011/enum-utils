/*
Enum Utils: Header-only C++17 library provides static reflection for enums.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "b110011/enum_utils/iterator.hpp"

#define B110011_TEST_CASE_PREFIX EnumIterator
#include "b110011/tests/catch2.hpp"

#include <array>

/*
Test plan:

Done    1. Enumeration
Done    2. Mask
*/

namespace b110011 {
namespace enum_utils {

TEST_CASE( "1. Enumeration", "[Utils][Enum][EnumIterator]" )
{
    //--------------------------------- Init ---------------------------------//

    enum Test { A, B, C, Count };

    std::array< Test, 3 > const ethalon{ A, B, C };
    std::size_t idx{ 0 };

    //--------------------------------- Check --------------------------------//

    EnumIterator< Test >::forEach(
        A,
        Count,
        [ & ] ( Test _value ) -> bool
        {
            REQUIRE( ethalon.at( idx++ ) == _value );
            return true;
        }
    );

    CHECK( idx == 3 );
}

TEST_CASE( "2. Mask", "[Utils][Enum][EnumIterator]" )
{
    //--------------------------------- Init ---------------------------------//

    enum Test
    {
        A       = 1 << 3,
        B       = 1 << 4,
        C       = 1 << 5,
        D       = 1 << 6,
        E       = 1 << 7,
        F       = 1 << 8,
        G       = 1 << 9,

        Last,
        First   = A
    };

    std::array< Test, 7 > const etalon{ A, B, C, D, E, F, G };
    std::size_t idx{ 0 };

    //--------------------------------- Check --------------------------------//

    EnumIterator< Test >::forEach(
        First,
        Last,
        [ & ] ( Test _value ) -> bool
        {
            REQUIRE( etalon.at( idx++ ) == _value );
            return true;
        }
    );

    CHECK( idx == 7 );
}

} // namespace enum_utils
} // namespace b110011
