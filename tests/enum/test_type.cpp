/*
Enum Utils: Header-only C++17 library provides static reflection for enums.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "b110011/enum_utils/type.hpp"

#define B110011_TEST_CASE_PREFIX EnumType
#include "b110011/tests/catch2.hpp"

/*
Test plan:

Done    1. Unknown
Done    2. Enumeration
Done    3. Mask
Done        3.1. Defined only 'First' element
Done        3.2. Defined only 'Last' element
Done        3.3. Defined 'First' and 'Last' elements
Done    4. Unspecified behavior
Done        4.1. First, Count -> Enumeration
Done        4.2. Last, Count -> Enumeration
Done        4.3. First, Last, Count -> Mask
*/

namespace b110011 {
namespace enum_utils {

template< typename _Enum >
void testEnumType ( EnumTypes _expectedEnumType )
{
    CHECK( EnumType< _Enum >::Value == _expectedEnumType );
    CHECK( EnumType< _Enum const >::Value == _expectedEnumType );

    CHECK( EnumType< _Enum & >::Value == _expectedEnumType );
    CHECK( EnumType< _Enum const & >::Value == _expectedEnumType );

    CHECK( EnumType< _Enum * >::Value == _expectedEnumType );
    CHECK( EnumType< _Enum const * >::Value == _expectedEnumType );

    CHECK( EnumType< _Enum volatile >::Value == _expectedEnumType );
    CHECK( EnumType< _Enum volatile const >::Value == _expectedEnumType );

    CHECK( EnumType< _Enum volatile & >::Value == _expectedEnumType );
    CHECK( EnumType< _Enum volatile const & >::Value == _expectedEnumType );

    CHECK( EnumType< _Enum volatile * >::Value == _expectedEnumType );
    CHECK( EnumType< _Enum volatile const * >::Value == _expectedEnumType );
}

TEST_CASE( "1. Unknown", "[Utils][Enum][EnumType]" )
{
    enum Test { A, B, C };

    testEnumType< Test >( EnumTypes::Unknown );
}

TEST_CASE( "2. Enumeration", "[Utils][Enum][EnumType]" )
{
    enum Test { A, B, C, Count };

    testEnumType< Test >( EnumTypes::Enumeration );
}

TEST_CASE( "3.1. Defined only 'First' element", "[Utils][Enum][EnumType][Mask]" )
{
    enum Test { A, B, C, First };

    testEnumType< Test > ( EnumTypes::Unknown );
}

TEST_CASE( "3.2. Defined only 'Last' element", "[Utils][Enum][EnumType][Mask]" )
{
    enum Test { A, B, C, Last };

    testEnumType< Test >( EnumTypes::Unknown );
}

TEST_CASE( "3.3. Defined 'First' and 'Last' elements", "[Utils][Enum][EnumType][Mask]" )
{
    enum Test { A, B, C, First, Last };

    testEnumType< Test >( EnumTypes::Mask );
}

TEST_CASE( "4.1. First, Count -> Enumeration", "[Utils][Enum][EnumType][Unspecified behavior]" )
{
    enum Test { A, B, C, First, Count };

    testEnumType< Test >( EnumTypes::Enumeration );
}

TEST_CASE( "4.2. Last, Count -> Enumeration", "[Utils][Enum][EnumType][Unspecified behavior]" )
{
    enum Test { A, B, C, Last, Count };

    testEnumType< Test >( EnumTypes::Enumeration );
}

TEST_CASE( "4.3. First, Last, Count -> Mask", "[Utils][Enum][EnumType][Unspecified behavior]" )
{
    enum Test { A, B, C, First, Last, Count };

    testEnumType< Test >( EnumTypes::Mask );
}

} // namespace enum_utils
} // namespace b110011
