/*
Enum Utils: Header-only C++17 library provides static reflection for enums.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "b110011/string/hashers.hpp"

#define B110011_TEST_CASE_PREFIX StringHashers
#include "b110011/tests/catch2.hpp"

/*
Test plan:

Done    1. Case sensitive
Done        1.1. Same data
Done        1.2. Different letter case
Done        1.3. Different data
Done    2. Case insensitive
Done        1.1. Same data
Done        1.2. Different letter case
Done        1.3. Different data
*/

namespace b110011 {

TEST_CASE( "1.1. Same data", "[Utils][String][Hashers][Case sensitive]" )
{
    CHECK( StringHasher< false >{}( "ABC" ) == StringHasher< false >{}( "ABC" ) );
}

TEST_CASE( "1.2. Different letter case", "[Utils][String][Hashers][Case sensitive]" )
{
    CHECK( StringHasher< false >{}( "ABC" ) != StringHasher< false >{}( "abc" ) );
}

TEST_CASE( "1.3. Different data", "[Utils][String][Hashers][Case sensitive]" )
{
    CHECK( StringHasher< false >{}( "HASH-LHS" ) != StringHasher< false >{}( "HASH-RHS" ) );
}

TEST_CASE( "2.1. Same data", "[Utils][String][Hashers][Case insensitive]" )
{
    CHECK( StringHasher< true >{}( "ABC" ) == StringHasher< true >{}( "ABC" ) );
}

TEST_CASE( "2.2. Different letter case", "[Utils][String][Hashers][Case insensitive]" )
{
    CHECK( StringHasher< true >{}( "abc" ) == StringHasher< true >{}( "ABC" ) );
    CHECK( StringHasher< true >{}( "AbC" ) == StringHasher< true >{}( "aBc" ) );
}

TEST_CASE( "2.3. Different data", "[Utils][String][Hashers][Case insensitive]" )
{
    CHECK( StringHasher< true >{}( "HASH-LHS" ) != StringHasher< true >{}( "HASH-RHS" ) );
    CHECK( StringHasher< true >{}( "HASH-lhs" ) != StringHasher< true >{}( "hash-RHS" ) );
}

} // namespace b110011
